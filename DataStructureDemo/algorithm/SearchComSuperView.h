//
//  SearchComSuperView.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SearchComSuperView : NSObject
+ (void)searchComSuperView:(UIView *)view otherView:(UIView *)otherView;
+ (NSMutableArray *)findViewController:(UIView *)selfView;//查找当前view的所有父视图
@end
