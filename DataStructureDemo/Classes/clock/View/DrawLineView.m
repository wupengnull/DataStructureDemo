//
//  DrawCyclesView.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "DrawLineView.h"
#import "DrawUtil.h"
#import "AnimationUtil.h"

@interface DrawLineView()
@property (nonatomic,strong) DrawLineViewConfig * config;
@end

@implementation DrawLineView

- (instancetype)initWithFrame:(CGRect)frame config:(DrawLineViewConfig *)config{
    self = [super initWithFrame:frame];
    if (self) {
        _config = config;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.backgroundColor = [UIColor whiteColor];
    
    CGRect frame = CGRectMake(0, 0, 200, 200);
    // Drawing code
    DrawUtil * drawUtil  = [[DrawUtil alloc] init];
    
    CAShapeLayer * solidLineLayer =[drawUtil drawSolidLine:CGPointMake(frame.origin.x+frame.size.width/2, frame.origin.y+frame.size.height/2) endPoint:CGPointMake(frame.origin.x+frame.size.width/2+_config.length, frame.origin.y+frame.size.height/2)];
    [self.layer addSublayer:solidLineLayer];

}


@end


@implementation DrawLineViewConfig

@end
