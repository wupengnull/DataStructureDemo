//
//  MainTabBarControllerConfig.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CYLTabBarController.h>

@interface MainTabBarControllerConfig : NSObject
@property (nonatomic,strong) CYLTabBarController * tabBarController;
@property (nonatomic, copy) NSString *context;
@end
