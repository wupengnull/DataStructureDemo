//
//  DrawCyclesView.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawLineViewConfig:NSObject
@property (nonatomic,assign) NSInteger length;
@end

@interface DrawLineView : UIView
- (instancetype)initWithFrame:(CGRect)frame config:(DrawLineViewConfig *)config;
@end
