//
//  StringReverse.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/10.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringReverse : NSObject
+ (void)reverse:(char [])cha;//字符串算法
+ (void)orderArrayMerge:(int [])a1 alen:(int)alen b:(int [])b1 blen:(int)blen;//有序数组合并
@end
