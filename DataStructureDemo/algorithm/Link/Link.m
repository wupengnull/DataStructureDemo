//
//  Link.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "Link.h"
#import "LinkNode.h"

@interface Link()
@property (nonatomic,strong) LinkNode * headerNode;//头结点
@end

@implementation Link

- (id)init{
    self = [super init];
    if (self) {
        _headerNode = [LinkNode new];
    }
    return self;
}

//追加尾部结点
- (void)appendNode:(NSString *)element headerNode:(LinkNode *)headerNode{
    LinkNode * newNode = [LinkNode new];
    newNode.element = element;
    
    LinkNode * p = headerNode;
    while (p.nextNode) {
        p = p.nextNode;
    }
    p.nextNode = newNode;
}
//插入到链表第一个
- (void)insertFirstNode:(LinkNode *)node headerNode:(LinkNode *)headerNode{
    node.nextNode = headerNode.nextNode;
    headerNode.nextNode = node;
}

//打印链表
- (void)printAllElement:(LinkNode *)headerNode{
    LinkNode * p = headerNode;
    NSString * linkStr = @"";
    while (p.nextNode) {
        p = p.nextNode;
        linkStr = [NSString stringWithFormat:@"%@,%@",linkStr,p.element];
    }
    NSLog(@"链表:%@",linkStr);

}

/*
 链表反转:从原链表的头部一个一个取节点并插入让到新链表的头部
 */
- (LinkNode *)linkReverse{
    NSLog(@"链表反转:");
    LinkNode * newHeaderNode = [LinkNode new];
    
    LinkNode * p = _headerNode;
    while (_headerNode.nextNode) {
        p = _headerNode.nextNode;
        _headerNode.nextNode = _headerNode.nextNode.nextNode;
        [self insertFirstNode:p headerNode:newHeaderNode];
    }

    return newHeaderNode;
    
}

- (void)printNode{
    [self appendNode:@"a" headerNode:_headerNode];
    [self appendNode:@"b" headerNode:_headerNode];
    [self appendNode:@"c" headerNode:_headerNode];
    [self appendNode:@"d" headerNode:_headerNode];
    [self appendNode:@"e" headerNode:_headerNode];
    [self appendNode:@"f" headerNode:_headerNode];
    
    [self printAllElement:_headerNode];
    
    LinkNode * newHeaderNode = [self linkReverse];
    [self printAllElement:newHeaderNode];
}

@end
