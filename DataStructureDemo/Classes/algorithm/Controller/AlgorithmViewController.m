//
//  AlgorithmViewController.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "AlgorithmViewController.h"
#import "StringReverse.h"
#import "A2.h"
#import "B.h"
#import "SearchComSuperView.h"
#import "Link.h"
#import "ClockViewController.h"
#import "LivePhotoViewController.h"
#import "FQPhotoAlbum.h"
#import "PageViewController.h"

@interface AlgorithmViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray * dataArr;
    @property (nonatomic, strong) FQPhotoAlbum      *photoAlbum;
@end

@implementation AlgorithmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"算法"];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = self.dataArr[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        char cha [] = "hello,world";
        [StringReverse reverse:cha];
        printf("result is %s \n",cha);
    }else if(indexPath.row == 1){
        A2 * a = [[A2 alloc] init];
        B * b = [[B alloc] init];
        [SearchComSuperView searchComSuperView:a otherView:b];
    }else if (indexPath.row == 2){
        int a1 [] = {1,4,6,7,9};
        int b1 [] = {2,3,5,6,8,10,11,12};
        
        int alen = sizeof(a1)/sizeof(int);
        int blen = sizeof(b1)/sizeof(int);
        
        [StringReverse orderArrayMerge:a1 alen:alen b:b1 blen:blen];
    }else if (indexPath.row == 3){
        Link * link = [[Link alloc] init];
        [link printNode];
    }else if (indexPath.row == 4){
        ClockViewController * clockVC = [[ClockViewController alloc] init];
        [self.navigationController pushViewController:clockVC animated:YES];
    }else if(indexPath.row == 5){
        if (!_photoAlbum) {
            _photoAlbum = [[FQPhotoAlbum alloc] init];
        }
        // 调用getPhotoAlbumOrTakeAPhotoWithController方法
        [_photoAlbum getPhotoAlbumOrTakeAPhotoWithController:self andWithBlock:^(UIImage *image) {
            //        self.iv.image = image;
        }];
        
//        LivePhotoViewController * liveVC = [[LivePhotoViewController alloc] initWithNibName:nil bundle:nil];
//        [self.navigationController pushViewController:liveVC animated:YES];
    }else if (indexPath.row == 6){
        PageViewController * pageVC = [[PageViewController alloc] init];
        [self.navigationController pushViewController:pageVC animated:YES];
    }
}

#pragma mark - getter
- (NSArray *)dataArr{
    if (!_dataArr) {
        _dataArr = @[@"字符串反转",@"寻找共同的父类",@"有序数组合并",@"链表操作",@"实现一个转盘时钟的绘制",@"live图片",@"阅读翻页"];
    }
    return _dataArr;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
