//
//  SearchComSuperView.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "SearchComSuperView.h"


@implementation SearchComSuperView

+ (void)searchComSuperView:(UIView *)view otherView:(UIView *)otherView{
    NSArray * viewArr = [self findViewController:view];
    NSArray * otherArr = [self findViewController:otherView];
    NSUInteger i = viewArr.count-1;
    NSUInteger j = otherArr.count-1;
    while (i>0 && j>0) {
        NSString * className = viewArr[i];
        NSString * otherName = otherArr[j];
        if ([className isEqualToString:otherName]) {
            NSLog(@"共同父类:%@",className);
        }
        i--;
        j--;
    }
}

+ (NSMutableArray *)findViewController:(UIView *)selfView{
    NSMutableArray<NSString *>* viewArr = [[NSMutableArray alloc] init];
    UIView * view = selfView;
    while (view) {
        NSString * className = NSStringFromClass([view class]);
        [viewArr addObject:className];
        view = view.superclass;
    }
    return viewArr;
}

@end
