//
//  LivePhotoViewController.m
//  DataStructureDemo
//
//  Created by ruantong on 2018/7/28.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "LivePhotoViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import <PhotosUI/PhotosUI.h>
#import "FQPhotoAlbum.h"

@interface LivePhotoViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic, strong) FQPhotoAlbum      *photoAlbum;
@end

@implementation LivePhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (!_photoAlbum) {
        _photoAlbum = [[FQPhotoAlbum alloc] init];
    }
    // 调用getPhotoAlbumOrTakeAPhotoWithController方法
    [_photoAlbum getPhotoAlbumOrTakeAPhotoWithController:self andWithBlock:^(UIImage *image) {
        //        self.iv.image = image;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        PHLivePhoto *photo = [info objectForKey:UIImagePickerControllerLivePhoto];
        if (photo) {
            PHLivePhotoView *photoView = [[PHLivePhotoView alloc]initWithFrame:self.view.bounds];
            photoView.livePhoto = [info objectForKey:UIImagePickerControllerLivePhoto];
            photoView.contentMode = UIViewContentModeScaleAspectFill;
            [self.view addSubview:photoView];
        } else {
            NSLog(@"普通图片,使用UIImageView加载即可");
        }
    }];
}
    
    
- (IBAction)readPhotoAction:(id)sender {
    if (!_photoAlbum) {
        _photoAlbum = [[FQPhotoAlbum alloc] init];
    }
    // 调用getPhotoAlbumOrTakeAPhotoWithController方法
    [_photoAlbum getPhotoAlbumOrTakeAPhotoWithController:self andWithBlock:^(UIImage *image) {
//        self.iv.image = image;
    }];
    
    
}
    
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
