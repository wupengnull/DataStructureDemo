//
//  StringReverse.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/10.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "StringReverse.h"

@implementation StringReverse
//字符串反转
+ (void)reverse:(char [])cha{
    char* begin = cha;
    char* end = cha+strlen(cha)-1;
    while (begin<end) {
        char temp = *begin;
        *(begin++) = *end;
        *(end--)=temp;
    }
}

//有序数组合并
+ (void)orderArrayMerge:(int [])a1 alen:(int)alen b:(int [])b1 blen:(int)blen{

    int c [13];
    
    int i = 0,j=0;
    int index = 0;
    while (i<alen && j<blen) {
        if (a1[i]<b1[j]) {
            c[index] = a1[i++];
        }else{
            c[index] = b1[j++];
        }
        index++;
    }
    
    while (i<alen) {
        c[index++]=a1[i++];
    }
    
    while (j<blen) {
        c[index++]=b1[j++];
    }

}

@end
