//
//  LinkNode.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinkNode : NSObject
@property (nonatomic,strong) LinkNode * nextNode;
@property (nonatomic,copy) NSString * element;
@end
