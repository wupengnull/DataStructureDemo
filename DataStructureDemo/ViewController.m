//
//  ViewController.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/10.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "ViewController.h"
#import "StringReverse.h"
#import "Network.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"切换到master");
    NSLog(@"分支2");
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"分支1");
}

- (void)viewWillDisappear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
