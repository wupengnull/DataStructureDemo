//
//  convertJsonDictUtil.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/14.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConvertJsonDictUtil : NSObject
//JSON字符串转化为字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
// 字典转json字符串方法
+(NSString *)convertToJsonData:(NSDictionary *)dict;
@end
