//
//  ShareSocialUtil.m
//  TaProject
//
//  Created by hello on 2018/6/14.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "ShareSocialUtil.h"
#import "OpenShareHeader.h"
#import <UIKit/UIKit.h>

@implementation ShareSocialUtil

//获取当前视图的viewcontroller
- (UIViewController *)getPresentedViewController
{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    if (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    
    return topVC;
}

- (void)shareAlert{
    
    UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:nil message:@"微信分享" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction * shareFriendAction = [UIAlertAction actionWithTitle:@"分享至朋友圈" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self shareToFriend];
    }];

    UIAlertAction * shareSessionAction = [UIAlertAction actionWithTitle:@"分享至好友列表" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self shareToWeixinSession];
    }];

    [actionSheetController addAction:cancelAction];
    [actionSheetController addAction:shareFriendAction];
    [actionSheetController addAction:shareSessionAction];
    
    
    // 显示
    [[self getPresentedViewController] presentViewController:actionSheetController animated:YES completion:nil];
}

//分享到朋友圈
- (void)shareToFriend{
    OSMessage *msg = [self shareMessage];
    
    [OpenShare shareToWeixinTimeline:msg Success:^(OSMessage *message) {
        NSLog(@"微信分享到朋友圈成功：\n%@",message);
    } Fail:^(OSMessage *message, NSError *error) {
        NSLog(@"微信分享到朋友圈失败：\n%@\n%@",error,message);
    }];
}

//分享给好友列表
- (void)shareToWeixinSession{
    OSMessage *msg = [self shareMessage];
    
    [OpenShare shareToWeixinSession:msg Success:^(OSMessage *message) {
        
    } Fail:^(OSMessage *message, NSError *error) {
        
    }];
}

- (OSMessage *)shareMessage {
    OSMessage *message = [[OSMessage alloc] init];
    message.title = [NSString stringWithFormat:@"幸福源(欣享)&爱相伴重疾计划书演示"];
    message.image = [UIImage imageNamed:@"shareImage"];
    // 缩略图
    message.thumbnail = [UIImage imageNamed:@"shareImage"];
    message.desc = [NSString stringWithFormat:@"幸福源(欣享)&爱相伴重疾计划书演示"];
    message.link=@"http://www.tianan-life.com/index";
    return message;
}
@end
