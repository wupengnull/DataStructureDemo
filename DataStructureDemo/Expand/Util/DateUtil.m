//
//  DateUtil.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/15.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "DateUtil.h"

@interface DateUtil()
{
    NSCalendar *calendar;
}

@end

@implementation DateUtil

//获取时、分、秒
- (int)getDateType:(dateType)type{
    if (!calendar) {
        calendar = [NSCalendar currentCalendar];
    }
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDate *now = [NSDate date];
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:now];
    if (type == hourType) {
        int hour = (int) [dateComponent hour];
        return hour;
    }else if(type == minuteType){
        int minute = (int) [dateComponent minute];
        return minute;
    }else{
        int second = (int)[dateComponent second];
        return second;
    }
    
}

@end
