//
//  NetworkViewController.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/14.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "NetworkViewController.h"
#import "Network.h"
#import "ConvertJsonDictUtil.h"
#import "OpenShareHeader.h"
#import "ShareSocialUtil.h"

@interface NetworkViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation NetworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"网络请求"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)benfitPermiumSAAction:(id)sender {
    NetworkConfig * config = [NetworkConfig new];
    config.urlString = @"http://210.13.77.68:19800/pd/products/benefitPremiumSA";
    
    [Network networkMethod:@"benefitPremiumSA.json" config:config block:^(id responseObject, NSError *error) {
        if (!error) {
            self.textView.text = @"保费计算结果返回成功";
        }else{
            self.textView.text = @"保费计算结果返回失败";
        }
    }];//计算保费或保额
}

- (IBAction)benefitIllustrationAction:(id)sender {
    NetworkConfig * config = [NetworkConfig new];
    config.urlString = @"http://210.13.77.68:19800/pd/products/benefitIllustration";
    
    [Network networkMethod:@"benefitIllustration.json" config:config block:^(id responseObject, NSError *error) {
        if (!error) {
            self.textView.text = @"利益展示结果返回成功";
        }else{
            self.textView.text = @"利益展示结果返回失败";
        }
    }];//利益展示
}

- (IBAction)openShareAction:(id)sender {
    ShareSocialUtil * sociaUtil = [[ShareSocialUtil alloc] init];
    [sociaUtil shareAlert];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
