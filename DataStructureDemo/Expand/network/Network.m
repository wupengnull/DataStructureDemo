//
//  Network.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "Network.h"
#import <AFNetworking/AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>

@implementation Network

#pragma mark - 网络请求

+ (void)networkMethod:(NSString *)fileName config:(NetworkConfig *)config block:(networkBlock)block{
    NSString *strPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:strPath];
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers error:nil];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //更改默认请求的发送请求的二进制数据为JSON格式的二进制更改默  认的序列化方式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    
    [manager.requestSerializer setValue:@"TIANAN" forHTTPHeaderField:@"X-ebao-tenant-id"];
    
    if(config.isSVProgressHUD){
        [SVProgressHUD show];
    }
    [manager POST:config.urlString parameters:dict progress:nil success:
     ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         block(responseObject,nil);
         NSLog(@"请求成功---%@---%@",responseObject,[responseObject class]);
         if(config.isSVProgressHUD){
             [SVProgressHUD dismiss];
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         block(nil,error);
         NSString *string =[[NSString alloc]initWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
         NSLog(@"%@",string);
         if(config.isSVProgressHUD){
             [SVProgressHUD dismiss];
         }
     }];
}
@end


@implementation NetworkConfig

- (instancetype)init{
    self = [super init];
    if(self){
        _isSVProgressHUD = YES;
    }
    return self;
}

@end
