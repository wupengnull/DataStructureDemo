//
//  ClockViewController.m
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import "ClockViewController.h"
#import "DrawLineView.h"
#import "Macros.h"
#import "AnimationUtil.h"
#import "DrawUtil.h"
#import "DateUtil.h"

@interface ClockViewController ()
{
    DrawLineView * hourLineView;
    DrawLineView * minuteLineView;
    DrawLineView * secondLineView;
    NSTimer * timer;
    DateUtil * dateUtil;
}
@end

@implementation ClockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"时钟"];

    CGRect frame = CGRectMake(200, 200, 200, 200);
    
    hourLineView =[self createLineView:frame length:40];
    [self.view addSubview:hourLineView];//绘制时
    
    minuteLineView =[self createLineView:frame length:60];
    [self.view addSubview:minuteLineView];//绘制时
    
    secondLineView = [self createLineView:frame length:80];
    [self.view addSubview:secondLineView];//绘制时
    
    DrawUtil * drawUtil  = [[DrawUtil alloc] init];
    [self.view.layer addSublayer:[drawUtil drawSolidArc:frame]];//绘制圆
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(animationTimer) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [timer invalidate];
}


- (void)animationTimer{
    NSLog(@"%@",[NSThread currentThread]);
    if (hourLineView && minuteLineView) {
        if (!dateUtil) {
            dateUtil = [[DateUtil alloc] init];
        }
        
        CABasicAnimation * hourRotateAnimation = [AnimationUtil rotateAnimation:([dateUtil getDateType:hourType]%12)/12.00*360 toValue:([dateUtil getDateType:hourType]%12)/12.00*360];
        [hourLineView.layer addAnimation:hourRotateAnimation forKey:@"rotate-layer"];
        
        CABasicAnimation * minuteRotateAnimation = [AnimationUtil rotateAnimation:[dateUtil getDateType:minuteType]/60.00*360 toValue:[dateUtil getDateType:minuteType]/60.00*360];
        [minuteLineView.layer addAnimation:minuteRotateAnimation forKey:@"rotate-layer"];
        
        CABasicAnimation * secondRotateAnimation = [AnimationUtil rotateAnimation:[dateUtil getDateType:secondType]/60.00*360 toValue:[dateUtil getDateType:secondType]/60.00*360];
        [secondLineView.layer addAnimation:secondRotateAnimation forKey:@"rotate-layer"];

    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - getter
- (DrawLineView *)createLineView:(CGRect)frame length:(NSInteger)length{
    DrawLineViewConfig * config = [[DrawLineViewConfig alloc] init];
    config.length = length;
    
    DrawLineView * lineView = [[DrawLineView alloc] initWithFrame:frame config:config];
    lineView.backgroundColor = [UIColor clearColor];
    
    return lineView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
