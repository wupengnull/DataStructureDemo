//
//  Network.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/12.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^networkBlock)(id responseObject,NSError * error);

@interface NetworkConfig : NSObject
@property (nonatomic,assign) BOOL isSVProgressHUD;//是否加载圈圈，默认加载
@property (nonatomic,copy) NSString * urlString;//请求url
@end

@interface Network : NSObject
+ (void)networkMethod:(NSString *)fileName config:(NetworkConfig *)config block:(networkBlock)block;
@end
