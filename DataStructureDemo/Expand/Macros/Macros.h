//
//  Macros.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/13.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ScreenWidth     [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight    [[UIScreen mainScreen] bounds].size.height
