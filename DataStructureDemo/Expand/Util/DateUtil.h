//
//  DateUtil.h
//  DataStructureDemo
//
//  Created by hello on 2018/6/15.
//  Copyright © 2018年 wupeng. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM (NSInteger, dateType)   {
    hourType,
    minuteType = 1,
    secondType = 2
};

@interface DateUtil : NSObject
//获取时、分、秒
- (int)getDateType:(dateType)type;
@end
